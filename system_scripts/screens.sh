#!/bin/bash
#
# a simple dmenu session script 
#
HDMI1_RES=$(wlr-randr  --output HDMI-A-1 && wlr-randr | grep HDMI-A-1 | sed -n 's/.* connected \([0-9]*\)x\([0-9]*\)+.*/\1x\2/p')
# HDMI1_RES='1600x900'
MENU='rofi -dmenu'
choice=$(echo -e "pantalla-secundaria\npantalla-duplicada\ndesactivar-secundaria\ndesactivar-primaria" | $MENU | cut -f 1)

case "$choice" in
  pantalla-secundaria) wlr-randr --output eDP-1 --on --output HDMI-A-1 --on;;
  pantalla-duplicada) wlr-randr --output eDP-1 --mode $HDMI1_RES;;
  desactivar-secundaria) wlr-randr --output eDP-1 --on --output HDMI-A-1 --off;;
  desactivar-primaria) wlr-randr --output eDP-1 --off --output HDMI-A-1 --on;;
esac

