#!/bin/bash
# based on https://github.com/b4skyx/scmenu
# Check whether the dependencies have been installed or not
type slurp &>/dev/null || {
	echo "Couldn't find slurp, which is required to take screenshots." >&2
	exit 17
}
type grim &>/dev/null || {
	echo "Couldn't find grim, which is required to take screenshots." >&2
	exit 17
}
type swappy &>/dev/null || {
	echo "Couldn't find swappy, which is required to edit screenshots." >&2
	exit 17
}

# Fallback to dmenu if rofi is not found
dmenu_cmd="rofi -dmenu"
type rofi&>/dev/null || {
	type dmenu&>/dev/null || {
		echo "Neither rofi nor dmenu installed..." >&2
		exit 17
	}
	dmenu_cmd="dmenu"
}


# Help message
function helpfn() {
	echo "WScmenu, a screenshot wrapper built around grim slurp and swappy based on scmenu by b4skyx" >&2
	echo "Menu navigation through dmenu/rofi" >&2
	echo "Note: Make sure you have the dependencies installed" >&2
	echo "Usage: Put this script in your path and bind your desired key for quick launch"
}

# Set inital options and choices

save_folder=${XDG_PICTURES_DIR}
options=("Fullscreen\nSelection\nTimer")
choice=""

# Update choice as required
function get_choice() {
	choice=$(echo -e "$options" | $dmenu_cmd -p "Select Type")
}

# Processing the choices made by the user
function runner() {
	file_name=$(date +Screenshot_%d%m%y_%H%M%S.png)
	if [ "$choice" = "Timer" ]
	then
		seconds=`$dmenu_cmd -p " After number of seconds"`
		choice=$(echo -e "Fullscreen\nSelection" | $dmenu_cmd -p "Type")
		sleep "$seconds"
		runner
	fi
	if [ "$choice" = "Fullscreen" ]
	then
		grim -l 7 -t png /tmp/screenshot.png
		save_clip
	elif [ "$choice" = "Selection" ]
	then
    grim -l 7 -t png -g "$(slurp)" /tmp/screenshot.png
		save_clip
	fi
}

# Function to save the screenshot
function save_clip(){
	options=("Copy to Clipboard\nSave Image\nEdit")
	choice=$(echo -e "$options" | $dmenu_cmd -p "Select Type")

	if [ "$choice" = "Copy to Clipboard" ]
	then
		wl-copy -t image/png < /tmp/screenshot.png
		notify-send -i /tmp/screenshot.png "Screenshot Captured!!" "Copied to clipboard"
	elif [ "$choice" = "Save Image" ]
	then
		notify-send -i /tmp/screenshot.png "Screenshot Saved!!" "$file_name"
		mv /tmp/screenshot.png $save_folder/$file_name
	elif [ "$choice" = "Edit" ]
	then
		notify-send -i /tmp/screenshot.png -i "Editing image!"
		mv /tmp/screenshot.png $save_folder/$file_name
		swappy -f $save_folder/$file_name 
	fi
}


if [ "$1" == "-h" -o "$1" == "--help" ]; then
	helpfn
	exit 0
elif [ $# -eq 0 ]; then
	get_choice
	runner
else
	echo "Bad arguments passed.">&2
fi
