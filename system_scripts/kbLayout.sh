#!/bin/bash
#
# a simple dmenu keyboard layout script
#
MENU='rofi -dmenu'
choice=$(echo -e "us\nlatam\notro" | $MENU | cut -f 1)

case "$choice" in
  us) riverctl keyboard-layout us;;
  latam) riverctl keyboard-layout latam;;
  otro) riverctl keyboard-layout us;;
esac

