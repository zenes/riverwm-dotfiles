# RiverWM dotfiles

## Dependences
- nerdfonts
- ranger
- nsxiv
- waybar
- swaybg
- dunst
- river
- pipewire
- nm-applet
- bluez
- thunar
- thumbler
- xarchiver
- qutebrowser
- rofi
- network-manager
- lunarvim
- neovim
- firefox
- gammastep
- wlr-randr
- wdisplays
- slurp
- grim
- swappy
