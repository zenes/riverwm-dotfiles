#!/usr/bin/env bash
# auto starting apps
killall -q polkit-gnome-authentication-agent-1 waybar dunst nm-applet gammastep blueman-applet
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
waybar -c ~/.config/waybar/river/config-river -s ~/.config/waybar/river/river_style.css &
sh ~/.config/system_scripts/pkill_bc &
nm-applet &
#swayidle -w timeout 300 ~/.config/system_scripts/wayland_session_lock &
blueman-applet &
playerctl daemon &
/usr/libexec/xdg-desktop-portal -r & /usr/libexec/xdg-desktop-portal-wlr &
dunst -config ~/.config/dunst/dunstrc &
xdg-user-dirs-update &
